import keycloak from "../keycloak";
import { Navigate } from "react-router-dom";
import 'keycloak-js';

function RoleCheckRoute({ children, role, redirectTo = "/restricted" }) {
  
  if (!!keycloak.hasResourceRole (role)) {
    return (<>{children}</>);
  } else { return <Navigate replace to={redirectTo} />;
  }
};

export default RoleCheckRoute;
