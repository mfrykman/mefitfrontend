import keycloak from "../keycloak"
import { Navigate } from "react-router-dom"


function LoggedInRoute({ children, redirectTo = "/" }) {

    if (!keycloak.authenticated) {
        return <Navigate replace to={redirectTo} />;
    }

    if (!!keycloak.authenticated) {
        return <> {children} </>;
    }

    return <Navigate replace to={redirectTo} />;
}

export default LoggedInRoute;