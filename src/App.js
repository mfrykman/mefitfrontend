import { BrowserRouter, Route, Routes } from "react-router-dom";
import StartPage from "./pages/StartPage";
import ProfilePage from "./pages/profile/ProfilePage";
import DashboardPage from "./pages/DashboardPage";
import ContributorPage from "./pages/ContributorPage";
import RestrictedContentPage from "./pages/RestrictedContentPage";
import LoggedInRoute from "./routes/LoggedInRoute";
import RoleCheckRoute from "./routes/RoleCheckRoute";
import Navbar from "./components/navbar/Navbar";
import TrainingPage from "./pages/training/TrainingPage";
import TrainingExercisesPage from "./pages/training/TrainingExercisesPage";
import TrainingProgramsPage from "./pages/training/TrainingProgramsPage";
import TrainingWorkoutsPage from "./pages/training/TrainingWorkoutsPage";
import './App.css'
import ExerciseDetailsPage from "./components/training/lists/ExerciseDetailsPage";
import CreateExercise from "./components/contributor_area/exercise/CreateExercise";
import Exercise from "./components/contributor_area/exercise/Exercise";
import Workout from "./components/contributor_area/workouts/Workout";
import Program from "./components/contributor_area/programs/Program";
import CreateWorkout from "./components/contributor_area/workouts/CreateWorkout";
import CreateProgram from "./components/contributor_area/programs/CreateProgram";
import WorkoutDetailsPage from "./components/training/lists/WorkoutDetailsPage";
import ProgramDetailsPage from "./components/training/lists/ProgramDetailsPage";

function App() {
  return (
    <BrowserRouter>
    <Navbar />
    <Routes>
      <>
        <Route path="/" element={<StartPage />} />

        <Route path="/dashboard" element={
          <LoggedInRoute>     <DashboardPage />   </LoggedInRoute>
        } />

        <Route path="/training" element={
        <LoggedInRoute>       <TrainingPage />    </LoggedInRoute>}> 

        <Route path="exercises" element={<TrainingExercisesPage />} />
        <Route path="workouts" element={<TrainingWorkoutsPage />} />
        <Route path="programs" element={<TrainingProgramsPage />} />

        </Route>      

        <Route path="training/exercises/exercise/:exerciseId" element={<ExerciseDetailsPage />}></Route>
        <Route path="training/workouts/workout/:workoutId" element={<WorkoutDetailsPage />}></Route>
        <Route path="training/programs/program/:programId" element={<ProgramDetailsPage />}></Route>

        <Route path="/profile" element={
          <LoggedInRoute>
            <ProfilePage />
          </LoggedInRoute>
        } />

        <Route path="contributor" element={
          <RoleCheckRoute role= 'Admin'>
            <ContributorPage />
          </RoleCheckRoute>}>
          <Route path="exercises" element={<Exercise />}/>
          <Route path="workouts" element={<Workout />}/>
          <Route path="programs" element={<Program />}/>
        </Route>

        <Route path="contributor/exercises/create-exercise" element={
          <LoggedInRoute>
            <CreateExercise />
          </LoggedInRoute>
        } />

        <Route path="contributor/workouts/create-workout" element={
          <LoggedInRoute>
            <CreateWorkout />
          </LoggedInRoute>
        } />

        <Route path="contributor/programs/create-program" element={
          <LoggedInRoute>
            <CreateProgram />
          </LoggedInRoute>
        } />

        <Route path="/restricted" element={<RestrictedContentPage />} />
        </>
</Routes>
    </BrowserRouter>
  );
}

export default App;