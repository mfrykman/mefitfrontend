import { Link } from "react-router-dom";
import keycloak from "../../keycloak";
import '../../App.css';


function Navbar() {

  const container = {};

  return (
    <nav>
      
        <div className="navbar">
          <div>
          <ul>
            
            <li>
              <div className="mainmenubutton">
            {!!keycloak.authenticated && 
            ( <Link to="/dashboard"><p className="LinkText">Dashboard</p></Link> )}
            </div>
            </li>

            <li>
            <div className="mainmenubutton">
            {!!keycloak.authenticated && 
            ( <Link to="/training"><p className="LinkText">Training</p></Link> )}
            </div>
            </li>
            
            <li>
            <div className="mainmenubutton">
            {!!keycloak.authenticated && 
            ( <Link to="/profile"><p className="LinkText">Profile</p></Link> )}
                        </div>

            </li>

            <li>
            <div className="mainmenubutton">
            {!!keycloak.hasResourceRole ('Admin' || 'Contributor') && 
            ( <Link to="/contributor"><p className="LinkText">Contributors Area</p></Link> )}
                        </div>

            </li>
           
            

          </ul>
          </div>
          {!!keycloak.authenticated && (
            <ul>
              <li>
            {!!keycloak.authenticated && 
            ( <> Logged in as: {keycloak.tokenParsed.name} </> )}
            </li>
              <li>
                <button className="button-9" onClick={() => keycloak.logout()}>Logout</button>
              </li>
            </ul>
          )}
        </div>
      
    </nav>
  );
}
export default Navbar;
