import keycloak from "../../keycloak";
import {useEffect, useState} from "react";
import {getProfile} from "../../api/profile";
import ProfileHeight from "./ProfileHeight";
import ProfileWeight from "./ProfileWeight";

function Profile(){

    const [profile, setProfile] = useState([]);

    useEffect(() => {
        async function fetchProfile() {
            let profile = await getProfile()
            setProfile(profile)
        }
        fetchProfile()
    }, [])

    return(
        <div className="container2">
            <div className="menutitle">
                <h1>Profile Page</h1>
                <h4>User</h4>
                <p>Name: { keycloak.tokenParsed.name}</p>
                <p>Username: { keycloak.tokenParsed.preferred_username}</p>
                <p>E-mail: {keycloak.tokenParsed.email}</p>
                <p>{profile && <ProfileHeight userProfile={profile}/>}</p>
                <p>{profile && <ProfileWeight userProfile={profile}/>}</p>
                <p>Roles: {JSON.stringify(keycloak.tokenParsed.resource_access.MeFit.roles)}</p>

            </div>
            <button style={{margin:"25px"}} onClick={keycloak.updateToken(30)}>Update Token</button>


        </div>
    )
}

export default Profile;