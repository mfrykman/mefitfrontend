import {useEffect, useState} from "react";
import { useForm } from 'react-hook-form'
import {getProfile, hasProfile, postProfile} from "../../api/profile";

function CreateProfile() {
    const {
        register,
        handleSubmit,
        formState: {errors}
    } = useForm()

    const onSubmit = async (data) => {
        const postStatus = await postProfile(data)
        if (postStatus === true) {
            window.alert("A profile has been created")
            window.location.reload(false)
        } else {
            window.alert("Profile couldn't be saved")
        }
    }


    return (
        <div>
            <h2>Create profile</h2>
            <form onSubmit={handleSubmit(onSubmit)} id="profileForm">
                <fieldset className="Form">
                    <label htmlFor="profileForm">Profile:</label>
                    <label htmlFor="weightId">Weight:</label>
                    <input id="weightId" placeholder="Height" defaultValue={0}
                           {...register("Weight", {min: {value: 35, message:"You must weigh at least 35 kg"}})} />

                    <label htmlFor="heightInput">Height:</label>
                    <input id="heightInput" type="number" placeholder="Weight" defaultValue={0}
                           {...register("Height", {min: {value: 100, message:"You must have a height of at least 100 cm"}})} />

                    <label htmlFor="medicalInput">Medical Conditions:</label>
                    <input id="medicalInput" type="text" placeholder="Medical Conditions" defaultValue=""
                           {...register("MedicalConditions", {required: "Write 'None' if you have no medical conditions"})} />

                    <label htmlFor="disabilitiesInput">Disabilities:</label>
                    <input id="disabilitiesInput" type="text" placeholder="Disabilities" defaultValue=""
                           {...register("Disabilities", {required: "Write 'None' if you have no disabilities"})} />

                    <button type="submit">Submit</button>
                </fieldset>
                {errors.Height && <p>{errors.Height.message}</p>}
                {errors.Weight && <p>{errors.Weight.message}</p>}
                {errors.MedicalConditions && <p>{errors.MedicalConditions.message}</p>}
                {errors.Disabilities && <p>{errors.Disabilities.message}</p>}
            </form>
        </div>
    )

}

export default CreateProfile;