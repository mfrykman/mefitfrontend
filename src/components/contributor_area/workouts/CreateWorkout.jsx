// Imports:
import { useState } from "react" 
import { useForm } from "react-hook-form"
import { useNavigate } from "react-router-dom";
import { getExerciseNames, getExercises } from "../../../api/exercise";
import { postWorkout } from "../../../api/workout";

// Create constructor like syntax:
const useConstructor = (callBack = () => {}) => {
    const [hasBeenCalled, setHasBeenCalled] = useState(false);
    if (hasBeenCalled) return;
    callBack();
    setHasBeenCalled(true);
}

// Set up register config
const nameConfig = {
    required:true,
    maxLength: 50,
    pattern: /^[a-zA-Z ]+$/g // Only letters
}

// Workout component:
function CreateWorkout() {
    // Hooks:
    const [exercisesSelectable, setExercisesSelectable] = useState([])
    const [exerciseNames, setExerciseNames] = useState([])
    const [exercises, setExercise] = useState([])
    const navigate = useNavigate();
 
    // Constructor (only runs on initialization)
    useConstructor(async () => {
        // Get exercise names from database
        const exercises = await getExercises()
        setExerciseNames(exercises.map(x => x.name))
        setExercise(exercises)

        // Create bool for each exercise.
        setExercisesSelectable(Array(exercises.length).fill(true))
    });

    // Set up react-form-hook commands: 
    const {
        register,
        handleSubmit,
        formState: {errors}
    } = useForm()

    // Triggers when select occurs
    const onSelect = (item) => {
        const selectedIndex = item.target.options.selectedIndex;
        const key = item.target.options[selectedIndex].getAttribute('refkey');

        let exerciseBool = exercisesSelectable
        exerciseBool[key] = false
        setExercisesSelectable(exerciseBool)
        setExerciseNames([...exerciseNames])
    }

    // Triggers when selected element is removed.
    const removeExercise = (index) => {
        let exerciseBool = exercisesSelectable
        exerciseBool[index] = true
        setExerciseNames([...exerciseNames])
    }

    // Submits data
    const onSubmit = async event => {
        let exerciseIds = []

        for (let i = 0; i < exerciseNames.length; i++) {
            if (exercisesSelectable[i] === false) {exerciseIds.push(exercises[i].exerciseId)}
        }
        
        const data = {
            Name: event.Name,
            Description: event.Description
        }

        const postStatus = await postWorkout(data, exerciseIds)

        if (postStatus === true) {
            navigate(-1)
        } 
        else {
            window.alert("Exercise couldn't be saved")
        }
    }

    return (
        <div>
            <button className="goback-button" onClick={() => navigate(-1)}>Go back</button>
            <h2>Create workout</h2>
            <p>
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <label htmlFor="workoutForm">Workout</label>
                    <br/>
                    <span>Name: </span>
                    <input type={"text"} placeholder="Workout name" {...register("Name", nameConfig)}></input>
                    <br/>
                    <span>Description: </span>
                    <input type={"text"} placeholder="Description" {...register("Description", nameConfig)}></input>
                    <br/>
                    <span>Exercises</span>
                    <select onChange={onSelect}>
                        <option value = "" disabled selected hidden>Exercises</option>
                        {exerciseNames.map((x,index) => exercisesSelectable[index] && <option key = {index} refkey = {index}>{x}</option>)}
                    </select>
                    <br/>
                    <button type="submit">Submit form</button>
                </fieldset>
                <div>{
                    exerciseNames.map((val,index) => 
                        !exercisesSelectable[index] && 
                        <div key = {index}>
                            <span>{val}</span> 
                            <button type="button" onClick = { () => removeExercise(index) }> X </button>
                        </div>
                        )    
                }</div>
            </form>
            </p>
        </div>
    )
} export default CreateWorkout