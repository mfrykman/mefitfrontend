import { Link } from "react-router-dom";
import { useState } from "react";
import { getUserObjects } from "../../../api/user";

// Create constructor like syntax:
const useConstructor = (callBack = () => {}) => {
  const [hasBeenCalled, setHasBeenCalled] = useState(false);
  if (hasBeenCalled) return;
  callBack();
  setHasBeenCalled(true);
};

function Workout() {
  const [workoutNames, setWorkoutNames] = useState([])
  const [workoutIds, setWorkoutIds] = useState([])

  useConstructor(async () => {
    const workouts = await getUserObjects("workouts");

    setWorkoutNames(workouts.map(x => x.name))
    setWorkoutIds(workouts.map(x => x.workoutId))
  })

  return (
    <div className="">
      <h3>Your workouts:</h3>
      <ul>
      { workoutNames.map((x, index)=> <Link key = {index} to = {`../../training/workouts/workout/${workoutIds[index]}`}><p className="list-p">{x}</p></Link>) }
        <div className="sublinkbox">
          <Link to="create-workout">
            {" "}
            <br /><br />
            <p className="sublink2">Create new workout</p>
          </Link>
        </div>
      </ul>
  </div>
  );
}


export default Workout;