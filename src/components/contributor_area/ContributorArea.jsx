import {
    Link,
    Outlet
  } from "react-router-dom";

function ContributorArea() {

    return(
        <div style={{textAlign:'center'}}>
        <ul className="submenu">
            <Link to = {"exercises"}><p className="sublink"> Exercises </p></Link>
            <Link to = {"workouts"}><p className="sublink">  Workouts </p></Link>
            <Link to = {"programs"}><p className="sublink">  Programs </p></Link>
        </ul>
            <Outlet/>
        </div>
    )
} 
export default ContributorArea
