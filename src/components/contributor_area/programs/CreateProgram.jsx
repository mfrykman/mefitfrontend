// Imports:
import { useState } from "react" 
import { useForm } from "react-hook-form"
import { getWorkoutNames, getWorkouts } from "../../../api/workout";
import { postProgram } from "../../../api/program";
import { useNavigate } from "react-router-dom";

// Create constructor like syntax:
const useConstructor = (callBack = () => {}) => {
    const [hasBeenCalled, setHasBeenCalled] = useState(false);
    if (hasBeenCalled) return;
    callBack();
    setHasBeenCalled(true);
}

// Set up register config
const nameConfig = {
    required:true,
    maxLength: 50,
    pattern: /^[a-zA-Z ]+$/g // Only letters
}

// program component:
function CreateProgram() {
    // Hooks:
    const [workoutsSelectable, setWorkoutsSelectable] = useState([])
    const [workoutNames, setWorkoutNames] = useState([])
    const [workouts, setWorkouts] = useState([])
    const navigate = useNavigate();
 
    // Constructor (only runs on initialization)
    useConstructor(async () => {
        // Get workout names from database
        const workouts = await getWorkouts()
        setWorkoutNames(workouts.map(x => x.name))
        setWorkouts(workouts)

        // Create bool for each workout.
        setWorkoutsSelectable(Array(workouts.length).fill(true))
    });

    // Set up react-form-hook commands: 
    const {
        register,
        handleSubmit,
        formState: {errors}
    } = useForm()

    // Triggers when select occurs
    const onSelect = (item) => {
        const selectedIndex = item.target.options.selectedIndex;
        const key = item.target.options[selectedIndex].getAttribute('refkey');

        let workoutBool = workoutsSelectable
        workoutBool[key] = false
        setWorkoutsSelectable(workoutBool)
        setWorkoutNames([...workoutNames])
    }

    // Triggers when selected element is removed.
    const removeWorkout = (index) => {
        let workoutBool = workoutsSelectable
        workoutBool[index] = true
        setWorkoutNames([...workoutNames])
    }

    // Submits data
    const onSubmit = async event => {
        let workoutIds = []

        for (let i = 0; i < workoutNames.length; i++) {
            if (workoutsSelectable[i] === false) {workoutIds.push(workouts[i].workoutId)}
        }

        const data = {
            Name: event.Name,
            Description: event.Description
        }

        const postStatus = await postProgram(data, workoutIds)

        if (postStatus === true) {
            navigate(-1)
        }
        else {
            window.alert("Exercise couldn't be saved")
        }
    }

    return (
        <div>
            <button className="goback-button" onClick={() => navigate(-1)}>Go back</button>
            <h2>Create program</h2>
            <p>
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <label htmlFor="programForm">Program</label>
                    <br/>
                    <span>Name: </span>
                    <input type={"text"} placeholder="Program name" {...register("Name", nameConfig)}></input>
                    <br/>
                    <span>Description: </span>
                    <br/>
                    <input type={"text"} placeholder="Description" {...register("Description", nameConfig)}></input>
                    <br/>
                    <select onChange={onSelect}>
                        <option value = "" disabled selected hidden>workouts</option>
                        {workoutNames.map((x,index) => workoutsSelectable[index] && <option key = {index} refkey = {index}>{x}</option>)}
                    </select>
                    <br/>
                    <button type="submit">Submit form</button>
                </fieldset>
                <div>{
                    workoutNames.map((val,index) => 
                        !workoutsSelectable[index] && 
                        <div key = {index}>
                            <span>{val}</span> 
                            <button type="button" onClick = { () => removeWorkout(index) }> X </button>
                        </div>
                        )    
                }</div>
            </form>
            </p>
        </div>
    )
} export default CreateProgram