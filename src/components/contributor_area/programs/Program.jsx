import { Link } from "react-router-dom"
import { useState } from "react"
import { getUserObjects } from "../../../api/user";

// Create constructor like syntax:
const useConstructor = (callBack = () => {}) => {
    const [hasBeenCalled, setHasBeenCalled] = useState(false);
    if (hasBeenCalled) return;
    callBack();
    setHasBeenCalled(true);
}

function Program() {
    const [programNames, setProgramNames] = useState([])
    const [programIds, setProgramIds] = useState([])

    useConstructor(async () => {
        const programs = await getUserObjects("Programs")
        
        setProgramNames(programs.map(x => x.name))
        setProgramIds(programs.map(x => x.programId))
    });

    return (
        <div className="">
            <h3>Your programs:</h3>
            <ul>
                { programNames.map((x, index)=> <Link key = {index} to = {`../../training/programs/program/${programIds[index]}`}><p className="list-p">{x}</p></Link>) }
            
            <div className="sublinkbox">  <br /><br />

            <Link to = "create-program"><p className="sublink2"> Create new program</p></Link>
            </div>
            </ul>
        </div>
    )
} 

export default Program