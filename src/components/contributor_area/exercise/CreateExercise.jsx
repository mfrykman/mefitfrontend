import { useState } from "react" 
import { useForm } from 'react-hook-form'
import { postExercise } from "../../../api/exercise"
import { useNavigate } from "react-router-dom";

const nameConfig = {
    required:true,
    maxLength: 50,
    pattern: /^[a-zA-Z ]+$/g // Only letters
}

const numConfig = {
    required:true,
    maxLength: 10,
    pattern: [0-9]
}

const selectConfig = {
    required:true
}

const urlConfig = {
    required:true
}

const textConfig = {
    required:true,
    maxLength: 750
}

const typeOptions = ["Distance", "Repetitions", "Timed"]
const muscleOptions = ["Calves", "Quadriceps", "Hamstrings", "Gluteus", "Loin", "Lats", "Abdominals", "Triceps", "Biceps", "Forearms", "Pectorals", "Deltoids"]

function CreateExercise() {
    const {
        register,
        handleSubmit, 
        formState: {errors}
    } = useForm()

    const [type, setType] = useState(0);
    const [exerciseType, setExerciseType] = useState("");
    const [exerciseData, setExerciseDate] = useState({Name: null, MuscleGroup: null, MuscleVariable: null, ImageLink: null, VideoLink: null, Description: null });
    const navigate = useNavigate();

    const onSubmit = async text => {
        const data = await {
            Name: text.Name,
            MuscleGroups: [text.MuscleGroup],
            Type: text.Type,
            ImageLink: text.imageLink,
            VideoLink: text.videoLink,
            Description: text.description,
            DistanceInKm: text.DistanceInKm,
            Repititions: text.Repititions,
            Seconds: text.Seconds
        }

        console.log(data)
        
        const postStatus = await postExercise(data)

        if (postStatus === true) {
            navigate(-1)
        }
        else {
            window.alert("Exercise couldn't be saved")
        }
    }

    const onSelect = item => {
        const index = item.target.options.selectedIndex;
        setType(index)
    }

    const errorMessage = (() => {
        if (!errors.nameText) {
            return null
        }
        if (errors.nameText.type === "required") {
            return (<p>Text input is required</p>)
        }
    })()

    return (
        <div>
            <button className="goback-button" onClick={() => navigate(-1)}>Go back</button>
             <h2>Create exercise</h2>
             <p>
             <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset className="Form">
                    <label htmlFor = "exerciseForm"> Your exercises: </label>
                    <span>Name: </span>
                    <input type="text" placeholder="Exercise name" {...register("Name", nameConfig)} />
                    <span>Type: </span>
                    <select {...register("Type", selectConfig)} onChange={onSelect}>
                        <option value = "" disabled selected hidden>Type</option>
                        {typeOptions.map((x, index) => <option key = {index}>{x}</option>)}
                    </select>
                    {type === 1 && 
                    <>
                        <span>Distance [km]</span>
                        <input type="text" {...register("DistanceInKm", numConfig)}  Value={0}/>
                    </>}
                    {type === 2 && 
                    <>
                        <span>Repititions</span>
                        <input type="text" {...register("Repititions", numConfig)} Value={0}/>
                    </>}
                    {type === 3 && 
                    <>
                        <span>Time [s]</span>
                        <input type="text" {...register("Seconds", numConfig)} Value={0}/>
                    </>}
                    <span>Muscle group: </span>
                    <select {...register("MuscleGroup", selectConfig)}>
                        <option value = "" disabled selected hidden>Muscle group</option>
                        {muscleOptions.map((x, index) => <option key = {index}>{x}</option>)}
                    </select>
                    <span>Image link: </span>
                    <input type="text" placeholder="Image link" {...register("imageLink", urlConfig)} />
                    <span>Video link: </span>
                    <input type="text" placeholder="Video link" {...register("videoLink", urlConfig)} />
                    <span>Description: </span>
                    <input type="text" placeholder="Description" {...register("description", textConfig)} />
                    <button type="submit">Submit</button>
                </fieldset>
                {errorMessage}
             </form>
             </p>
        </div>
    )
} 

export default CreateExercise