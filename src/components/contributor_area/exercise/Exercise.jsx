import { useState } from "react"
import { Link } from "react-router-dom"
import { getUserObjects } from "../../../api/user";

// Create constructor like syntax:
const useConstructor = (callBack = () => {}) => {
    const [hasBeenCalled, setHasBeenCalled] = useState(false);
    if (hasBeenCalled) return;
    callBack();
    setHasBeenCalled(true);
}

function Exercise() {
    const [exerciseNames, setExercisesNames] = useState([])
    const [exerciseIds, setExerciseIds] = useState([])

    useConstructor(async () => {
        const exercises = await getUserObjects("exercises")

        setExercisesNames(exercises.map(x => x.name))
        setExerciseIds(exercises.map(x => x.exerciseId))

    });

    return (
        <div className="">
            <h3>Your exercises:</h3>
            <ul>
                { exerciseNames.map((x,index)=> <Link  key = {index} to = {`../../training/exercises/exercise/${exerciseIds[index]}`}><p className="list-p">{x}</p></Link>) }
            
            <div className="sublinkbox">            <br /><br />

            <Link to = "create-exercise"><p className="sublink2"> Create new exercise</p></Link>
            </div>
            </ul>
        </div>
    )
} export default Exercise 