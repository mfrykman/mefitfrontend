import { useNavigate } from "react-router-dom";
import { useParams } from "react-router";
import { getExerciseById } from "../../../api/exercise";
import { useState } from "react";

function ExerciseDetailsPage() {

  const { exerciseId } = useParams();
  const x = parseInt(exerciseId);

  const [constructorBool, setConstructorBool] = useState(true);
  const [data, setData] = useState([]);

  createData();

  async function createData() {
    if (constructorBool) {
      let exercise = await getExerciseById(x);
      setData(exercise);
      setConstructorBool(false);
    }
  }

  console.log(x);

  const navigate = useNavigate();

  return (
    <>
      <button className="goback-button" onClick={() => navigate(-1)}>Go back</button>

      <div>
        <h1>Exercise details for {data.name}</h1>
        <h3>{data.description}</h3>
     
        <p>
          <img alt="IMG" src={data.imageLink} />
        </p>
        <p>
          <iframe
            title="video"
            width="420"
            height="315"
            src={data.videoLink}
          ></iframe>
        </p>
      </div>
    </>
  );
}

export default ExerciseDetailsPage;

//    <h3>Target muscles: {data.muscleGroups}</h3>