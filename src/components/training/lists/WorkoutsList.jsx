import { getWorkouts } from "../../../api/workout"
import { Link } from "react-router-dom";
import { useState } from "react";

const submenu = {
    font: "Courier New",
  padding: "",
  margin: "5px",
  color: "bisque",
  fontSize: "15px",
    };


    function WorkoutsList() {

        const [constructorBool, setConstructorBool] = useState(true)
        const [data, setData] = useState([])

        createData()

        async function createData() {
            if (constructorBool) {
                let workouts = await getWorkouts()
                setData(workouts)

                setConstructorBool(false)
            }
        }

        return (

           <>
                <div className="tableparent">
                {data
                .sort((a, b) => (a.workoutId - b.workoutId))
                .map((x, index) => 
                <div className="tablechild">
                <Link to={`workout/${x.workoutId}`} style={submenu} align="left" key={index}><p className="list-p">{x.name}</p></Link>
                </div>
                )}
                </div>
           </>
        
        )
        }
        

        

export default WorkoutsList;
