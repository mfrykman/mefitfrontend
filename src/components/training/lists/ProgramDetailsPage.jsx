import { Link, useNavigate } from "react-router-dom";
import { useParams } from "react-router";
import { getProgramById, getProgramWorkouts } from "../../../api/program";
import { useState } from "react";
import { getWorkouts } from "../../../api/workout";

function ProgramDetailsPage() {
  const submenu = {
    font: "Courier New",
    padding: "",
    margin: "5px",
    color: "bisque",
    fontSize: "15px",
  };

  const { programId } = useParams();
  const x = parseInt(programId);

  const [constructorBool, setConstructorBool] = useState(true);
  const [data, setData] = useState([]);
  const [dataEx, setDataEx] = useState([]);
  const [dataExNames, setDataExNames] = useState([]);

  createData();

  async function createData() {
    if (constructorBool) {
      let program = await getProgramById(x);
      let programWorkouts = await getProgramWorkouts(x);
      let workouts = await getWorkouts();

      setDataExNames(workouts);
      setDataEx(programWorkouts);
      setData(program);
      setConstructorBool(false);
    }
  }
  let filteredWorkouts = dataExNames.filter((x) =>
    dataEx.includes(x.workoutId)
  );

  console.log(filteredWorkouts);

  const navigate = useNavigate();

  return (
    <>
      <button className="goback-button" onClick={() => navigate(-1)}>Go back</button>

      <div>
        <h1>Program details for {data.name}</h1>
        <h2>Description: { x.description }</h2>
        <h3>This Program contains following Workouts:</h3>
        
        <div className="tableparent">
          {filteredWorkouts.map((x, index) => (
            <div className="tablechild">
                <Link
                  to={"/training/workouts/workout/" + `${x.workoutId}`}
                  style={submenu}
                  align="left"
                  key={index}
                >
                  <p className="list-p">{x.name}</p>
                </Link>
              </div>
          ))}
        </div>
      </div>
    </>
  );
}

export default ProgramDetailsPage;
