import { getPrograms } from "../../../api/program"
import { Link } from "react-router-dom";
import { useState } from "react";

const submenu = {
    font: "Courier New",
  padding: "",
  margin: "5px",
  color: "bisque",
  fontSize: "15px",
    };


    function ProgramsList() {

        const [constructorBool, setConstructorBool] = useState(true)
        const [data, setData] = useState([])

        createData()

        async function createData() {
            if (constructorBool) {
                let programs = await getPrograms()
                setData(programs)

                setConstructorBool(false)
            }
        }

        return (

           <>
                <div className="tableparent">
                {data
                .sort((a, b) => (a.programId - b.programId))
                .map((x, index) => 
                <div className="tablechild">
                <Link to={`program/${x.programId}`} style={submenu} align="left" key={index}><p className="list-p">{x.name}</p></Link>
                </div>
                )}
                </div>
           </>
        
        )
        }
        

        

export default ProgramsList;
