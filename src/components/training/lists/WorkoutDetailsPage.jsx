import { Link, useNavigate } from "react-router-dom";
import { useParams } from "react-router";
import { getWorkoutById, getWorkoutExercises } from "../../../api/workout";
import { useState } from "react";
import { getExercises } from "../../../api/exercise";

function WorkoutDetailsPage() {
  const submenu = {
    font: "Courier New",
    padding: "",
    margin: "5px",
    color: "bisque",
    fontSize: "15px",
  };

  const navigate = useNavigate();
  const { workoutId } = useParams();
  const x = parseInt(workoutId);

  const [constructorBool, setConstructorBool] = useState(true);
  const [data, setData] = useState([]);
  const [dataEx, setDataEx] = useState([]);
  const [dataExNames, setDataExNames] = useState([]);

 

  createData();

  async function createData() {
    if (constructorBool) {
      let workout = await getWorkoutById(x);
      let workoutexercises = await getWorkoutExercises(x);
      let exercises = await getExercises();

      setDataExNames(exercises);
      setDataEx(workoutexercises);
      setData(workout);
      setConstructorBool(false);
    }
  }
  let filteredexercises = dataExNames.filter((x) =>
    dataEx.includes(x.exerciseId)
  );

  console.log(filteredexercises);



  return (
    <>
      <button className="goback-button" onClick={() => navigate(-1)}>Go back</button>

      <div>
        <h1>Workout details for {data.name}</h1>
        <h3>This workout contains following exercises:</h3>
        <div className="tableparent">
          {filteredexercises.map((x, index) => (
            <div className="tablechild">
                <Link
                  to={"/training/exercises/exercise/" + `${x.exerciseId}`}
                  style={submenu}
                  align="left"
                  key={index}
                >
                 <p className="list-p"> {x.name}</p>
                </Link>
                </div>
          ))}
        </div>
      </div>
    </>
  );
}

export default WorkoutDetailsPage;
