import { Link } from "react-router-dom";
import { getExercises } from "../../../api/exercise";
import { useState } from "react";


const submenu = {
    font: "Courier New",
  padding: "",
  margin: "5px",
  color: "bisque",
  fontSize: "15px",
    };


    function ExercisesList() {

        const [constructorBool, setConstructorBool] = useState(true)
        const [data, setData] = useState([])

        createData()

        async function createData() {
            if (constructorBool) {
                let exercises = await getExercises()
                setData(exercises)

                setConstructorBool(false)
            }
        }

        return (

           <>
                <div className="tableparent">
                {data
                .sort((a, b) => (a.exerciseId - b.exerciseId))
                .map((x, index) => 
                <div className="tablechild">
                <Link to={`exercise/${x.exerciseId}`} style={submenu} align="left" key={index}><p className="list-p">{x.name}</p></Link>
                </div>
                )}
                </div>
           </>
        
        )
        }
        

export default ExercisesList;











/*

<p key = {index}>{x}</p>


  data?.map((data, index) => {
        
                return (
                    <tr>
                        <td><Link to={`exercise/${index + 1}`} style={submenu}>{data?.name}</Link></td>
                    </tr>
                )})

            



return (
    
    <div className="">
    
    {data}

    </div>

    
)}


export default ExercisesList;





const url = 'http://minira-backend.herokuapp.com/api/Exercises/Exercises/';

const { data, loading, error } = useFetch(url);

if (loading) return <h1>Loading...</h1>;
if (error) console.log(error);

const exercises_arr = data.map((data, index) => {
    

    return (
        <tr>
            <td><Link to={`exercise/${index + 1}`} style={submenu}>{data.Name}</Link></td>
        </tr>
    )})


return (
    <div className="">

    {exercises_arr}

    </div>

    
)}
*/
