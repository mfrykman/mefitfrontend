// Imports:
import {useEffect, useState} from "react"
import { useForm } from "react-hook-form"
import { postGoal } from "../api/goal";
import { getProgramNames, getPrograms } from "../api/program";
import Moment from 'moment';
import DatePicker from 'react-date-picker';
import keycloak from "../keycloak";
import { getProfile } from "../api/profile";

// Create constructor like syntax:
const useConstructor = (callBack = () => {}) => {
    const [hasBeenCalled, setHasBeenCalled] = useState(false);
    if (hasBeenCalled) return;
    callBack();
    setHasBeenCalled(true);
}

const dateConfig = {
    required:true,
    valueAsDate: true,
}

// Set up register config
const nameConfig = {
    required:true,
    maxLength: 50,
    pattern: /^[a-zA-Z]+$/g // Only letters
}

// goal component:
function CreateGoal(props) {
    // Hooks:
    const [programNames, setProgramNames] = useState([])
    const [programs, setPrograms] = useState([])
    const [programId, setProgramId] = useState()
    const formatDate = Moment(new Date()).format('DD-MM-YYYY')
    const [startDate, setStartDate] = useState(new Date())
    const [endDate, setEndDate] = useState(new Date())
    const [profile, setProfile] = useState([]);

    useEffect(() => {
        async function fetchProfile() {
            let profile = await getProfile()
            setProfile(profile)
        }
        fetchProfile()
    }, [])

    // Constructor (only runs on initialization)
    useConstructor(async () => {
        // Get program names from database
        const programs = await getPrograms()
        setPrograms(programs)
        setProgramNames(programs.map(x => x.name))
    });

    // Set up react-form-hook commands: 
    const {
        register,
        handleSubmit,
        formState: {errors}
    } = useForm()

    // Triggers when select occurs
    const onSelect = (item) => {
        const selectedIndex = item.target.options.selectedIndex;
        const key = item.target.options[selectedIndex].getAttribute('refkey');

        setProgramId(key)
    }

    // Submits data
    const onSubmit = async event => {

        const data = {
            StartingDate: Moment(startDate).format('DD/MM/YYYY'),
            EndDate: Moment(endDate).format('DD/MM/YYYY'),
            ProfileId: profile.profileId,
            CompletedWorkouts: []
        }
        if (!data.ProfileId) {
            window.alert("No profile has been crseated")
            return 
        }

        const postStatus = await postGoal(data, programId)

        if (postStatus != null) {
            window.alert("New goal was added")
            await props.updateGoals(postStatus)
        }
        else {
            window.alert("Could not save goal")
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <label htmlFor="goalForm"><h4>Add new goal:</h4></label>
                    <select onChange={onSelect}>
                        <option value = "" disabled selected hidden>programs</option>
                        {programNames.map((x,index) => <option key = {index} refkey = {index}>{x}</option>)}
                    </select>
                    <span>Start date:</span>
                    <DatePicker onChange={setStartDate} value={startDate}/>
                    <span>End date:</span>
                    <DatePicker onChange={setEndDate} value={endDate}/>
                    <button className="goal-button" type="submit">Add goal</button>
                </fieldset>
            </form>
        </div>
    )
} export default CreateGoal