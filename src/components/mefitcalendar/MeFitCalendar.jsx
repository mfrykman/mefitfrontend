import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import format from "date-fns/format";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import getDay from "date-fns/getDay"
import "react-big-calendar/lib/css/react-big-calendar.css";
import React from "react";
import 'moment/locale/nb';

const locales = {
    "en-GB": require("date-fns/locale/en-GB")
}

const localizer = dateFnsLocalizer({
    format,
    parse,
    getDay,
    startOfWeek,
    locales
})

const events = [
    {
        title: "Core Workout",
        allDay: true,
        start: new Date(2022, 10, 0),
        end: new Date(2022, 10, 3)
    }
]

function MeFitCalendar() {
return (
    <div className="calendar">
        <Calendar localizer={localizer} events={events}  defaultView="day" views={["week", "day"]}
        startAccessor="start" endAccessor="end" style={{height: 200, margin: "50px"}} />
    </div>
);
}

export default MeFitCalendar;