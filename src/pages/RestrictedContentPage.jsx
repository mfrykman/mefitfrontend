import keycloak from "../keycloak";

function RestrictedContentPage() {


    return (
      <div>
      <h1>Contributors Area</h1>
      { keycloak.authenticated &&
       <div>

         Apply to become a contributor on your profile page.
         
       </div>
      }
    </div>
    );
  }
  export default RestrictedContentPage;
  