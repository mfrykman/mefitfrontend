import keycloak from "../../keycloak";
import CreateProfile from "../../components/profile/CreateProfile";
import {useEffect, useState} from "react";
import {getProfile} from "../../api/profile";
import Profile from "../../components/profile/Profile";

function ProfilePage() {
    const [profile, setProfile] = useState(null);

    useEffect(() => {
        async function fetchProfile() {
            let profile = await getProfile()
            setProfile(profile)

            console.log("Profile:", profile);
        }
        fetchProfile()
    }, [])


    return (
        <div>
            <Profile/>

            {!profile && <CreateProfile/>}
        </div>


    );
}
export default ProfilePage;
