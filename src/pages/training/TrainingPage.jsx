import { Link, Outlet } from "react-router-dom";


function TrainingPage() {

  

  return (
    <div style={{textAlign:'center'}}>
      <h1>Training</h1>
       <div  className="submenu">
       <ul><p className="trainingmenu">
       <Link to="exercises"><p className="sublink">Exercises       </p></Link>
       <Link to="workouts"><p className="sublink">Workouts       </p></Link>
       <Link to="programs"><p className="sublink">Programs</p></Link>
       </p>
       <Outlet />
       
      </ul>
          
        </div>
    </div>
  );
}
export default TrainingPage;
