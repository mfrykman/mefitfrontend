import ProgramsList from "../../components/training/lists/ProgramsList";

function TrainingProgramsPage() {

    return (
      <div>
        <h3>Programs</h3>
         <div>
            <ProgramsList />
            
          </div>
      </div>
    );
  }
  export default TrainingProgramsPage;
  