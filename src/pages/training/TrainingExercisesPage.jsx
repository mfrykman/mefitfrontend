import ExercisesList from "../../components/training/lists/ExercisesList";

function TrainingExercisesPage() {

    return (
      <div>
        <h3>Exercises</h3>
         <div>
            <ExercisesList />
            
          </div>
      </div>
    );
  }
  export default TrainingExercisesPage;
  