import WorkoutsList from "../../components/training/lists/WorkoutsList";

function TrainingWorkoutsPage() {

    return (
      <div>
        <h3>Workouts</h3>
         <div>
            <WorkoutsList />
            
          </div>
      </div>
    );
  }
  export default TrainingWorkoutsPage;
  