import { useState } from "react";
import { Link } from "react-router-dom";
import { deleteGoal, getGoals, getGoalsById } from "../api/goal";
import { getUserObjects } from "../api/user";
import CreateGoal from "../components/Goal";
import MeFitCalendar from "../components/mefitcalendar/MeFitCalendar";
import ProgressBar from "../components/progressbar/ProgressBar";
import React from "react";

const testData = [
    { bgcolor: "#ef6c00", completed: 53 },
];

// Create constructor like syntax:
const useConstructor = (callBack = () => {}) => {
  const [hasBeenCalled, setHasBeenCalled] = useState(false);
  if (hasBeenCalled) return;
  callBack();
  setHasBeenCalled(true);
}

function DashboardPage() {
  const [goalNames, setGoalNames] = useState([])
  const [startTime, setStartTime] = useState()
  const [endTime, endStartTime] = useState()
  const [checked, setChecked] = useState([])
  const [dataRows, setDataRows] = useState([])
  const [goals, setGoals] = useState([])
  

   // Constructor (only runs on initialization)
   useConstructor(async () => {
      const goalsFetch = await getUserObjects("goals")
      const goalsFetch2 = goalsFetch.filter(x=> x.program != null).filter(x=> x.program.name != null)
      setGoals(goalsFetch2)

      let rows = []

      for (let i = 0; i < goalsFetch2.length; i++) {
        rows.push(goalsFetch2[i].program.workouts.length)
      }
        
      setDataRows(rows)  
      setChecked(Array(goalsFetch2.length).fill(0))
      });

  const addChecked = (event) => {
    const row = event.target.getAttribute('row')
    let rowCount = checked

    if(event.target.checked) {
      rowCount[row]++
    } else {
      rowCount[row]--
    }
    setChecked(rowCount)
    setGoals([...goals])
  }

  const updateGoals = async (id) => {
      console.log(id)
      const newGoal = await getGoalsById(id)
      if (newGoal === null) {
        return
      }
      console.log(newGoal)
      setGoals([...goals, newGoal])

      setDataRows([...dataRows, newGoal.program.workouts.length])  
      setChecked([...checked, 0])
  };

  const DeleteGoal = async (event) => {
    const key = await event.target.getAttribute('refkey')
    console.log(key)

     await deleteGoal(key)
  }

  return (
    <div className="goals-container">
      <div style={{textAlign:'center'}}>
      <h1>Goals</h1>

       {
       goals.map((x,index) => 
       <div key = {index}>
        <Link to = {`../training/programs/program/${x.program.programId}`}><span className="span-link">{x.program.name}</span></Link>
        {dataRows[index] === checked[index] && <span>&#10004;  </span>}
        <span className="time">{`Time span: ${x.startingDate}-${x.endDate} `}</span>
        <button key = {index} className="delete-button"  refkey = {x.goalId} onClick={DeleteGoal}>Delete</button>
        <br/>
        {x.program.workouts.map((y, index2) => 
        <div className="workoutLinks">
          <Link to = {`../training/workouts/workout/${y.workoutId}`}>{y.name}</Link>
          <input type="checkbox" key={index2} onChange={addChecked} row = {index}></input>
          <br/>
        </div>)}
       </div>)
       }
      
      <CreateGoal updateGoals = {updateGoals} />
      <ProgressBar className="ColorBar" completed  = {checked.filter((x, index) => x === dataRows[index]).reduce((a,b) => a + b, 0)*50/checked.length}  />
        
    </div>
    </div>
  );
}
export default DashboardPage;
