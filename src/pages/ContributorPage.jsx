import ContributorArea from "../components/contributor_area/ContributorArea";

function ContributorPage() {

  return (
<div className="container2">
    <div className="menutitle">
      <h1>Contributor Area</h1>   
      </div>
 
      <ContributorArea />
      </div>

  );
}
export default ContributorPage;
