import keycloak from "../keycloak";
import { useState } from "react";
import { postUser } from "../api/user"

// Create constructor like syntax:
const useConstructor = (callBack = () => {}) => {
  const [hasBeenCalled, setHasBeenCalled] = useState(false);
  if (hasBeenCalled || !keycloak.authenticated) return;
  callBack();
  setHasBeenCalled(true);
}

function StartPage() {

  // Constructor (only runs on initialization)
  useConstructor(async () => {
      await postUser()
  });

  return (
    <div className="center">
      <h1>Welcome to MeFit!!!</h1>

      <section className="actions">
        {!keycloak.authenticated && (
          <button className="button-8" onClick={() => keycloak.login()}>Login</button>
        )}
        
      </section>
      <br />
      <div>
                  <img alt="fuck" src={"./mefitlogo.png"} height="80px" width="150px" border="3px solid #73AD21" margin="10px" /> 
                </div>
      {keycloak.authenticated && (
        <div>
          <br />
          <h4>Check out your training area to get started!</h4>
          
        </div>
      )}
    </div>
  );
}
export default StartPage;
