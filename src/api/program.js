import { createHeaders, readHeaders } from "./programHeaders"

// Gets apiUrl
const apiUrl = process.env.REACT_APP_API_URL + "/programs"

// Collects previous translations and updates API with new ones.
export const postProgram = async (data, workouts) => {
    try {
        let response = await fetch(apiUrl, { // Accesses data for logged in user
            method: "POST", // Only update a single record
            headers: createHeaders(),
            mode: 'cors',
            body: JSON.stringify(data)
        })

        const postId = await response.json();
        await updateProgram(postId, workouts)
        
        return true
    }
    // Returns error if update was not successful.
    catch (error) {
        return error.message
    }
}

export const updateProgram = async (Id, workouts) => {
    try {
        const response = await fetch(`${apiUrl}/${Id}/workouts`, { // Accesses data for logged in user
            method: "PUT", // Only update a single record
            headers: createHeaders(),
            mode: 'cors',
            body: JSON.stringify(workouts)
        })

        return true
    }
    // Returns error if update was not successful.
    catch (error) {
        return error.message
    }
}

export const getProgramNames = async () => {
    try {
        const response = await fetch(apiUrl, {
            method: 'GET',
            mode: 'cors',
            headers: readHeaders()
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        const data = await response.json();
        return data.map(x => x.name)
    }
    catch (error) {
        return ["Error"]
    }
}

export const getPrograms = async () => {
    try {
        const response = await fetch(`${apiUrl}`, { // Accesses data for logged in user
            method: "GET", // Only update a single record
            mode: 'cors',
            headers: readHeaders()
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        const data = await response.json();
        return data
    }
    // Returns error if update was not successful.
    catch (error) {
        console.log(readHeaders())
        return [1,2,3,4,5,6,7,8,9,10]
    }
}

export const getProgramById = async (id) => {
    try {
        const response = await fetch(`${apiUrl}` + `/${id}`, { // Accesses data for logged in user
            method: "GET", // Only update a single record
            headers: readHeaders(),
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        const data = await response.json();

        return data
    }
    // Returns error if update was not successful.
    catch (error) {
        console.log(readHeaders())
        return [1,2,3,4,5,6,7,8,9,10]
    }
}

export const getProgramWorkouts = async (id) => {
    try {
        const response = await fetch(`${apiUrl}` + `/${id}` + "/workouts", { // Accesses data for logged in user
            method: "GET", // Only update a single record
            headers: readHeaders(),
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        const data = await response.json();

        return data
    }
    // Returns error if update was not successful.
    catch (error) {
        console.log(readHeaders())
        return [1,2,3,4,5,6,7,8,9,10]
    }
}