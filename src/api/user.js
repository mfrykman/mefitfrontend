import { headers } from "./headers";
import keycloak from "../keycloak";

const apiUrl = process.env.REACT_APP_API_URL + "/User"

export const postUser = async () => {
    try {
        const result = fetch(apiUrl, { // Accesses data for logged in user
            method: "POST", // Only update a single record
            headers: headers(),
            mode: 'cors'
        })

        return true
    }
    catch (error) {
        return error.message
    }
}

export const getUserObjects = async (input) => {
    try {
        const response = await fetch(`${apiUrl}/${input}`, {
            method: 'GET',
            mode: 'cors',
            headers: headers()
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        return await response.json()
    }
    // Returns error if update was not successful.
    catch (error) {
        return ["Error"]
    }
}