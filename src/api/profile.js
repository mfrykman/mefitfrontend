import { headers } from "./headers";
import {json} from "react-router-dom";

// Gets apiUrl
const apiUrl = process.env.REACT_APP_API_URL + "/Profiles";
const userApiUrl = process.env.REACT_APP_API_URL + "/User";

export const postProfile = async (profileData) => {
    try {
        const response = await fetch(apiUrl, {
            method: "POST", // Only update a single record
            headers: headers(),
            mode: 'cors',
            body: JSON.stringify(profileData)
        })
        return true;
    }
    // Returns error if update was not successful.
    catch (error) {
        return null
    }
}

export const putProfile = async (profileData) => {
    try {
        const response = await fetch(`${apiUrl}/${profileData.profileId}`, {
            method: "POST", // Only update a single record
            headers: headers(),
            mode: 'cors',
            body: JSON.stringify(profileData)
        })
        return null;
    }
        // Returns error if update was not successful.
    catch (error) {
        return error.message
    }
}

export const getProfile = async () => {
    try {
        const response = await fetch(`${userApiUrl}/Profile`, {
            method: "GET",
            headers: headers(),
            mode: 'cors',
        })

        let data = await response.json();
        return data;
    }

    catch (error) {
        return null;
    }
}

