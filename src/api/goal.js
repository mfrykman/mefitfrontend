import keycloak from "../keycloak";
import {headers} from "./headers";
import { createHeaders } from "./workoutHeaders";

// Gets apiUrl
const apiUrl = process.env.REACT_APP_API_URL + "/Goals"

// Collects previous translations and updates API with new ones.
export const postGoal = async (data) => {
    console.log("Data:", data)
    try {
        let response = await fetch(apiUrl, { // Accesses data for logged in user
            method: "POST", // Only update a single record
            headers: headers(),
            mode: 'cors',
            body: JSON.stringify(data)
        })

        const postId = await response.json();
        await updateGoal(postId, program)
        return postId
    }
    // Returns error if update was not successful.
    catch (error) {
        return null
    }
}

export const updateGoal = async (Id, program) => {
    try {
        await fetch(`${apiUrl}/${Id}/program`, { // Accesses data for logged in user
            method: "PUT", // Only update a single record
            headers: createHeaders(),
            mode: 'cors',
            body: JSON.stringify(program)
        })

        return true
    }
    // Returns error if update was not successful.
    catch (error) {
        return null
    }
}

export const getGoals = async () => {
    try {
        const response = await fetch(`${apiUrl}`, { // Accesses data for logged in user
            method: "GET", // Only update a single record
            mode: 'cors',
            headers: headers()
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        const data = await response.json();
        return data
    }
    // Returns error if update was not successful.
    catch (error) {
        return null
    }
}

export const getGoalsById = async (id) => {
    try {
        const response = await fetch(`${apiUrl}` + `/${id}`, { // Accesses data for logged in user
            method: "GET", // Only update a single record
            headers: createHeaders(),
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        const data = await response.json();

        return data
    }
    // Returns error if update was not successful.
    catch (error) {
        return null
    }
}

// Collects previous translations and updates API with new ones.
export const deleteGoal = async (id) => {
    try {
        let response = await fetch(`${apiUrl}/${id}`, { // Accesses data for logged in user
            method: "DELETE", // Only update a single record
            headers: headers(),
            mode: 'cors',
        })

        const postId = await response.json();
        console.log(postId)
        console.log(keycloak.token)
        return true
    }
    // Returns error if update was not successful.
    catch (error) {
        console.log("Hey")
        return null
    }
}