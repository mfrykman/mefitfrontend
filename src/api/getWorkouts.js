import { headers } from "./headers";

const apiUrl = process.env.REACT_APP_API_URL + "/Workouts/"


export const getWorkouts = async () => {
    try {
        const response = await fetch(apiUrl, { // Accesses data for logged in user
            method: 'GET',
            mode: 'cors',
            headers: headers()
        })
        
if(!response.ok) {
            throw new Error("Could not complete request")
        }
const data = await response.json();

return data;
    }
    // Returns error if update was not successful.
    catch (error) {
        return ["Fail"]
    }
}