import keycloak from "../keycloak"

export const headers = () => {
    return {
        'Content-Type' : 'application/json',
        'Authorization' : 'Bearer ' + keycloak.token
    }
}