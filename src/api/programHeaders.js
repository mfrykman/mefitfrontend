import keycloak from "../keycloak"
const apiKey = process.env.REACT_APP_API_KEY

export const createHeaders = () => {
    return {
        'Content-Type' : 'application/json',
        'Authorization' : 'Bearer ' + keycloak.token
    }
}

export const readHeaders = () => {
    return {
        'Content-Type' : 'application/json',
        'Authorization' : 'Bearer ' + keycloak.token
    }
}