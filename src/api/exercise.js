// Import guide patching specification for API update.
import { json } from "react-router-dom"
import { headers } from "./headers";

// Gets apiUrl
const apiUrl = process.env.REACT_APP_API_URL + "/Exercises"

// Collects previous translations and updates API with new ones.
export const postExercise = async (exerciseData) => {
    try {
        const response = await fetch(apiUrl, { // Accesses data for logged in user
            method: "POST", // Only update a single record
            headers: headers(),
            mode: 'cors',
            body: JSON.stringify(exerciseData)
        })

        return true
    }
    // Returns error if update was not successful.
    catch (error) {
        return null
    }
}

export const getExerciseNames = async () => {
    try {
        const response = await fetch(apiUrl, {
            method: 'GET',
            mode: 'cors',
            headers: headers()
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        const data = await response.json();
        return data.map(x => x.name)
    }
    // Returns error if update was not successful.
    catch (error) {
        return ["Error"]
    }
}


// get all exercises method

export const getExercises = async () => {
    try {
        const response = await fetch(apiUrl, { // Accesses data for logged in user
            method: 'GET',
            mode: 'cors',
            headers: headers()
        })
        
    if(!response.ok) {
            throw new Error("Could not complete request")
        }
    const data = await response.json();

    return data;
    }
    // Returns error if update was not successful.
    catch (error) {
        return ["Fail"]
    }
}

export const getExerciseById = async (id) => {
    try {
        const response = await fetch(apiUrl + `/${id}`, { // Accesses data for logged in user
            method: 'GET',
            mode: 'cors',
            headers: headers()
        })

    if(!response.ok) {
            throw new Error("Could not complete request")
        }
    const data = await response.json();

    return data;
    }
    // Returns error if update was not successful.
    catch (error) {
        return ["Fail"]
    }
}