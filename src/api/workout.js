// Import guide patching specification for API update.
import { createHeaders,readHeaders } from "./workoutHeaders"

// Gets apiUrl
const apiUrl = process.env.REACT_APP_API_URL + "/Workouts"

// Collects previous translations and updates API with new ones.
export const postWorkout = async (data, exercises) => {
    try {
        let response = await fetch(apiUrl, { // Accesses data for logged in user
            method: "POST", // Only update a single record
            headers: createHeaders(),
            mode: 'cors',
            body: JSON.stringify(data)
        })

        const postId = await response.json();
        await updateWorkout(postId, exercises)
        
        return true
    }
    // Returns error if update was not successful.
    catch (error) {
        console.log("what")
        return error.message
    }
}

export const updateWorkout = async (Id, exercises) => {
    try {
        const response = await fetch(`${apiUrl}/${Id}/exercises`, { // Accesses data for logged in user
            method: "PUT", // Only update a single record
            headers: createHeaders(),
            mode: 'cors',
            body: JSON.stringify(exercises)
        })

        const data = await response.json()
        return true
    }
    // Returns error if update was not successful.
    catch (error) {
        return error.message
    }
}

export const getWorkoutNames = async () => {
    try {
        const response = await fetch(apiUrl, {
            method: 'GET',
            mode: 'cors',
            headers: readHeaders()
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        const data = await response.json();
        return data.map(x => x.name)
    }
    // Returns error if update was not successful.
    catch (error) {
        return ["Error"]
    }
}

export const getWorkouts = async () => {
    try {
        const response = await fetch(`${apiUrl}`, { // Accesses data for logged in user
            method: "GET", // Only update a single record
            headers: readHeaders(),
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        console.log("hej")
        const data = await response.json();

        return data
    }
    // Returns error if update was not successful.
    catch (error) {
        console.log(readHeaders())
        return [1,2,3,4,5,6,7,8,9,10]
    }
}

export const getWorkoutById = async (id) => {
    try {
        const response = await fetch(`${apiUrl}` + `/${id}`, { // Accesses data for logged in user
            method: "GET", // Only update a single record
            headers: readHeaders(),
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        console.log("hej")
        const data = await response.json();

        return data
    }
    // Returns error if update was not successful.
    catch (error) {
        console.log(readHeaders())
        return [1,2,3,4,5,6,7,8,9,10]
    }
}

export const getWorkoutExercises = async (id) => {
    try {
        const response = await fetch(`${apiUrl}` + `/${id}` + "/Exercises", { // Accesses data for logged in user
            method: "GET", // Only update a single record
            headers: readHeaders(),
        })

        if(!response.ok) {
            throw new Error("Could not complete request")
        }

        console.log("hej")
        const data = await response.json();

        return data
    }
    // Returns error if update was not successful.
    catch (error) {
        console.log(readHeaders())
        return [1,2,3,4,5,6,7,8,9,10]
    }
}